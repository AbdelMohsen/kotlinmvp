package com.me.myapplication.di.annotations

import javax.inject.Qualifier

@Qualifier
@kotlin.annotation.Retention annotation class PreferencesInfo
