package com.me.myapplication.di.builder

import com.me.myapplication.ui.splash.SplashActivityModule
import com.me.myapplication.ui.splash.view.SplashMVPActivity
import com.me.myapplication.ui.test.TestFragmentProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(SplashActivityModule::class), (TestFragmentProvider::class)])
    abstract fun bindSplashActivity(): SplashMVPActivity

}