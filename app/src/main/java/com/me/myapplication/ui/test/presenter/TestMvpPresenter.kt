package com.me.myapplication.ui.test.presenter

import com.me.myapplication.ui.base.presenter.MvpPresenter
import com.me.myapplication.ui.test.interactor.TestMvpInteractor
import com.me.myapplication.ui.test.view.TestMvpView

interface TestMvpPresenter<V : TestMvpView, I : TestMvpInteractor> : MvpPresenter<V, I> {
    fun showToast();
}