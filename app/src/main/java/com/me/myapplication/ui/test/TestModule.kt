package com.me.myapplication.ui.test

import com.me.myapplication.ui.test.interactor.TestInteractor
import com.me.myapplication.ui.test.interactor.TestMvpInteractor
import com.me.myapplication.ui.test.presenter.TestMvpPresenter
import com.me.myapplication.ui.test.presenter.TestPresenter
import com.me.myapplication.ui.test.view.TestMvpView
import dagger.Module
import dagger.Provides

@Module
class TestModule {
    @Provides
    internal fun provideTestMvpInteractor(testInteractor: TestInteractor): TestMvpInteractor = testInteractor

    @Provides
    internal fun provideTestMvpPresenter(testPresenter: TestPresenter<TestMvpView, TestMvpInteractor>): TestMvpPresenter<TestMvpView, TestMvpInteractor> {
        return testPresenter
    }

}