package com.me.myapplication.ui.test.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.me.myapplication.R
import com.me.myapplication.ui.base.view.BaseFragment
import com.me.myapplication.ui.test.interactor.TestMvpInteractor
import com.me.myapplication.ui.test.presenter.TestMvpPresenter
import javax.inject.Inject

class TestFragment : BaseFragment(),TestMvpView {

    @Inject
    lateinit var presenter: TestMvpPresenter<TestMvpView, TestMvpInteractor>

    override fun getLayout(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.test_fragment, container, false)
    }


    override fun onItemClicked() {
    }

    override fun initializeComponent(view: View) {
        presenter.onAttach(this)
        presenter.showToast()
    }
}