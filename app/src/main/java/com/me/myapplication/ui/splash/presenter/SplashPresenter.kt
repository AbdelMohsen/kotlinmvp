package com.me.myapplication.ui.splash.presenter

import com.me.myapplication.ui.base.presenter.BaseMvpPresenter
import com.me.myapplication.ui.splash.interactor.SplashMVPInteractor
import com.me.myapplication.ui.splash.view.SplashMVPView
import javax.inject.Inject

/**
 * Created by Abdel mohsen on 04/01/18.
 */
class SplashPresenter<V : SplashMVPView, I : SplashMVPInteractor>
@Inject internal constructor(interactor: I)
    : BaseMvpPresenter<V, I>(interactor = interactor), SplashMVPPresenter<V, I> {
    override fun showToast() {
        getView()!!.showSuccessToast()
        interactor?.test()
    }

    override fun onAttach(view: V?) {
        super.onAttach(view)
    }

    private fun isUserLoggedIn(): Boolean {
        interactor?.let { return it.isUserLoggedIn() }
        return false
    }

}