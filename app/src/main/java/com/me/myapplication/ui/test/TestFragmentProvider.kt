package com.me.myapplication.ui.test

import com.me.myapplication.ui.test.view.TestFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class TestFragmentProvider {
    @ContributesAndroidInjector(modules = [(TestModule::class)])
    abstract internal fun provideTestFragment(): TestFragment
}