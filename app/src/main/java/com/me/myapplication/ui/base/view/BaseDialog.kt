package com.me.myapplication.ui.base.view

import android.content.Context
import android.support.v4.app.DialogFragment
import android.view.WindowManager
import android.widget.Toast
import com.me.myapplication.R
import com.me.myapplication.utils.AppConstants
import com.me.myapplication.utils.extention.ShowMessgae
import dagger.android.support.AndroidSupportInjection

abstract class BaseDialog : DialogFragment(), DialogMvpView {

    private lateinit var baseActivity: BaseActivity

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is BaseActivity) {
            val baseActivity = context as BaseActivity
            this.baseActivity = baseActivity

        }
    }

    override fun showMessage(message: String, toastDuration: AppConstants.ToastDuration) {
        val toast = Toast(activity)
        toast.ShowMessgae(activity as BaseActivity, message)
    }

    override fun onResume() {
        // Get existing layout params for the window
        val params = dialog.window!!.attributes
        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT
        params.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes = params as WindowManager.LayoutParams
        dialog.window!!.setWindowAnimations(R.style.DialogAnimation_2)
        super.onResume()
    }

    private fun performDependencyInjection() {
        AndroidSupportInjection.inject(this)
    }

    private fun getBaseActivity(): BaseActivity {
        return baseActivity
    }
}