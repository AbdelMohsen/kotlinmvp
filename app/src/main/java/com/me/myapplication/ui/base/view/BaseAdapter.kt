package com.me.myapplication.ui.base.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log

abstract class BaseAdapter<item>(protected var context: Context, var data: ArrayList<item?>?) :
        RecyclerView.Adapter<BaseViewHolder>() {


    protected val VIEW_TYPE_EMPTY = 0
    protected val VIEW_TYPE_NORMAL = 1

    private lateinit var onRecyclerItemClick: OnRecyclerItemClick

    fun setOnRecyclerItemClick(onRecyclerItemClick: OnRecyclerItemClick) {
        this.onRecyclerItemClick = onRecyclerItemClick
    }

    fun getOnRecyclerItemClick(): OnRecyclerItemClick {
        return onRecyclerItemClick
    }

    override fun getItemCount(): Int {
        return if (data == null && data?.size == 0) {
            1
        } else {
            data!!.size
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (data == null && data?.size == 0)
            VIEW_TYPE_EMPTY
        else
            VIEW_TYPE_NORMAL
    }

    fun insertAll(mData: List<item?>) {
        with(data) {
            this?.clear()
            this?.addAll(mData)
            Log.e("xxx", "InsertAll")
            notifyDataSetChanged()
        }
    }

    fun insert(position: Int, item: item?) {
        with(data) {
            this?.add(position, item)
            Log.e("xxx", "Insert one item")
            notifyDataSetChanged()
        }
    }

    fun delete(position: Int) {
        data?.removeAt(position)
        notifyDataSetChanged()
    }
}
