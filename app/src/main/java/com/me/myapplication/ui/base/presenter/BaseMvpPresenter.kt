package com.me.myapplication.ui.base.presenter

import com.me.myapplication.ui.base.iteractor.MvpInteractor
import com.me.myapplication.ui.base.view.MvpView

abstract class BaseMvpPresenter<V : MvpView, I : MvpInteractor>
    internal constructor(protected var interactor: I?) : MvpPresenter<V, I> {

    private var view: V? = null

    override fun onAttach(view: V?) {
        this.view = view;
    }

    override fun onDetach() {
        this.view = null
    }

    override fun getView(): V? = this.view
}