package com.me.myapplication.ui.splash.view

import com.me.myapplication.ui.base.view.ActivityMvpView

/**
 * Created by jyotidubey on 04/01/18.
 */
interface SplashMVPView : ActivityMvpView {

    fun showSuccessToast()
    fun showErrorToast()
}