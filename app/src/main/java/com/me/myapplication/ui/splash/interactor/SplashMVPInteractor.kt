package com.me.myapplication.ui.splash.interactor

import com.me.myapplication.ui.base.iteractor.MvpInteractor

/**
 * Created by jyotidubey on 04/01/18.
 */
interface SplashMVPInteractor : MvpInteractor {
    fun test()
}