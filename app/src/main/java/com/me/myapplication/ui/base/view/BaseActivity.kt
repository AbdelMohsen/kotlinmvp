package com.me.myapplication.ui.base.view

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.me.myapplication.R
import com.me.myapplication.utils.AppConstants
import com.me.myapplication.utils.extention.ShowMessgae
import com.me.myapplication.utils.extention.replaceFragment
import dagger.android.AndroidInjection

/**
 * Created by AbdEl mohsen on 21/09/18.
 */
abstract class BaseActivity : AppCompatActivity(), ActivityMvpView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)


        if (isFullScreen()){
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }

        setContentView(getLayoutResource())
        initializeComponents()
        onComponentClickListener()
    }


    override fun onResume() {
        super.onResume()
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out)
    }

    override fun onPause() {
        super.onPause()
        overridePendingTransition(R.anim.slide_out, R.anim.slide_in)
    }

    override fun getContext(): Activity {
        return this
    }

    override fun loadCurrentFragment(containerLayout: Int, fragment: Fragment,
                                     TAG: String, toolbarTitle: String?, enableToolBarIcons: Boolean,
                                     animationIn: Int, animationOut: Int) {
        supportFragmentManager.replaceFragment(containerLayout, fragment, TAG, toolbarTitle, enableToolBarIcons,
                animationIn, animationOut)

    }

    override fun showMessage(message: String, toastDuration: AppConstants.ToastDuration) {
        Toast(this).ShowMessgae(this, message, toastDuration)
    }

    protected abstract fun initializeComponents()

    protected abstract fun getLayoutResource(): Int

    protected abstract fun onComponentClickListener()

    protected abstract fun isFullScreen():Boolean

}