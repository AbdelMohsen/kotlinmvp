package com.me.myapplication.ui.base.view

import com.me.myapplication.utils.AppConstants

interface MvpView {

    fun showMessage(message: String, toastDuration: AppConstants.ToastDuration);

}