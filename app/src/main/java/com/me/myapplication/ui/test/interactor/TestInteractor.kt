package com.me.myapplication.ui.test.interactor

import android.content.Context
import com.me.myapplication.data.network.ApiHelper
import com.me.myapplication.data.preferences.PreferencesHelper
import com.me.myapplication.ui.base.iteractor.BaseInteractor
import javax.inject.Inject

class TestInteractor
@Inject constructor(context: Context, apiHelper: ApiHelper, preferencesHelper: PreferencesHelper)
    : BaseInteractor(preferencesHelper, apiHelper), TestMvpInteractor {
}