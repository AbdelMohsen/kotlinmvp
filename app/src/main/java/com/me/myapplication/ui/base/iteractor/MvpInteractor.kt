package com.me.myapplication.ui.base.iteractor

interface MvpInteractor {

    fun isUserLoggedIn(): Boolean

    fun performUserLogout()

}