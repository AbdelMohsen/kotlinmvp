package com.me.myapplication.ui.base.iteractor

import com.me.myapplication.data.network.ApiHelper
import com.me.myapplication.data.preferences.PreferencesHelper
import com.me.myapplication.utils.AppConstants

open class BaseInteractor() : MvpInteractor {

    protected lateinit var preferencesHelper: PreferencesHelper
    protected lateinit var apiHelper: ApiHelper

    internal constructor(preferencesHelper: PreferencesHelper, apiHelper: ApiHelper) : this() {
        this.preferencesHelper = preferencesHelper
        this.apiHelper = apiHelper
    }

    override fun isUserLoggedIn(): Boolean = this.preferencesHelper.getUserLoggedImMode() != AppConstants.LoggedImMode.LOGGED_IN_MODE_LOGGED_OUT.type

    override fun performUserLogout() {
        preferencesHelper.let {
            it.setUserAccessToken(null)
            it.setUserEmail(null)
            it.setUserId(AppConstants.INDEX_NULL)
            it.setUserLoggedInMode(AppConstants.LoggedImMode.LOGGED_IN_MODE_LOGGED_OUT)
            it.setUserName(null)
            it.setUserProfilePic(null)
        }
    }


}