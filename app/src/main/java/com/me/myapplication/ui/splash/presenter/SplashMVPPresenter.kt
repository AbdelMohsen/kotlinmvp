package com.me.myapplication.ui.splash.presenter

import com.me.myapplication.ui.base.presenter.MvpPresenter
import com.me.myapplication.ui.splash.interactor.SplashMVPInteractor
import com.me.myapplication.ui.splash.view.SplashMVPView

/**
 * Created by jyotidubey on 04/01/18.
 */
interface SplashMVPPresenter<V : SplashMVPView, I : SplashMVPInteractor> : MvpPresenter<V, I> {
    fun showToast()
}