package com.me.myapplication.ui.base.view

import android.support.v7.widget.RecyclerView
import android.view.View

abstract class BaseViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

    abstract fun onBind(position: Int)
    
}
