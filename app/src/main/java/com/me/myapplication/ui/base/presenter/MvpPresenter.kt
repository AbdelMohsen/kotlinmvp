package com.me.myapplication.ui.base.presenter

import com.me.myapplication.ui.base.iteractor.MvpInteractor
import com.me.myapplication.ui.base.view.MvpView

interface MvpPresenter<V : MvpView, I : MvpInteractor> {

    fun onAttach(view: V?)

    fun onDetach()

    fun getView(): V?
}