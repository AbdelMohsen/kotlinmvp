package com.me.myapplication.ui.test.presenter

import com.me.myapplication.ui.base.presenter.BaseMvpPresenter
import com.me.myapplication.ui.test.interactor.TestMvpInteractor
import com.me.myapplication.ui.test.view.TestMvpView
import com.me.myapplication.utils.AppConstants
import javax.inject.Inject

class TestPresenter<V : TestMvpView, I : TestMvpInteractor>
@Inject constructor(interactor: I) : BaseMvpPresenter<V, I>(interactor), TestMvpPresenter<V, I> {
    override fun showToast() {
        getView()!!.showMessage("hello from Fragment Presenter", AppConstants.ToastDuration.DURATION_LONG)
    }
}