package com.me.myapplication.ui.base.view

import android.app.Activity
import android.support.v4.app.Fragment
import com.me.myapplication.R

interface ActivityMvpView : MvpView {

    fun getContext(): Activity

    fun loadCurrentFragment(containerLayout: Int, fragment: Fragment,
                            TAG: String, toolbarTitle: String?, enableToolBarIcons: Boolean,
                            animationIn: Int = R.anim.slide_in, animationOut: Int = R.anim.slide_out)
}