package com.me.myapplication.ui.base.view

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.me.myapplication.utils.AppConstants
import com.me.myapplication.utils.extention.ShowMessgae
import dagger.android.support.AndroidSupportInjection

abstract class BaseFragment : Fragment(), MvpView {
    private var baseActivity: BaseActivity? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return getLayout(inflater, container, savedInstanceState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performInjection()
        setHasOptionsMenu(false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is BaseActivity) {
            val activity = context as BaseActivity?
            this.baseActivity = activity
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeComponent(view)
    }

    fun getBaseActivity() = baseActivity

    private fun performInjection() = AndroidSupportInjection.inject(this)

    override fun showMessage(message: String, toastDuration: AppConstants.ToastDuration) {
        Toast(baseActivity).ShowMessgae(baseActivity as BaseActivity, message, toastDuration)
    }

    protected abstract fun onItemClicked()

    protected abstract fun initializeComponent(view: View)

    protected abstract fun getLayout(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
}