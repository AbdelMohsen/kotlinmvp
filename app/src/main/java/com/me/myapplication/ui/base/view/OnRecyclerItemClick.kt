package com.me.myapplication.ui.base.view

import android.view.View

interface OnRecyclerItemClick {
    fun onItemClicked(view: View?, position: Int)
}