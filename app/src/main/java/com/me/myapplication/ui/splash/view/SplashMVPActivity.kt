package com.me.myapplication.ui.splash.view

import android.os.Bundle
import android.support.v4.app.Fragment
import com.me.myapplication.R
import com.me.myapplication.ui.base.view.BaseActivity
import com.me.myapplication.ui.splash.interactor.SplashMVPInteractor
import com.me.myapplication.ui.splash.presenter.SplashMVPPresenter
import com.me.myapplication.ui.test.view.TestFragment
import com.me.myapplication.utils.AppConstants
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class SplashMVPActivity : BaseActivity(), SplashMVPView, HasSupportFragmentInjector {


    @Inject
    lateinit var presenter: SplashMVPPresenter<SplashMVPView, SplashMVPInteractor>
    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<android.support.v4.app.Fragment>


    override fun initializeComponents() {

        loadCurrentFragment(R.id.fragment_container, TestFragment(), "TestFragment", "test fragment",
                false)

    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_main
    }

    override fun onComponentClickListener() {

    }

    override fun isFullScreen(): Boolean {
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onAttach(this)
        presenter.showToast()
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }


    override fun showSuccessToast() {
        showMessage("Hello Again ", AppConstants.ToastDuration.DURATION_SHORT)
    }

    override fun showErrorToast() {
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }

}
