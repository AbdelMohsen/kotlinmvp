package com.me.myapplication.ui.splash.interactor

import android.content.Context
import com.me.myapplication.data.network.ApiHelper
import com.me.myapplication.data.preferences.PreferencesHelper
import com.me.myapplication.ui.base.iteractor.BaseInteractor
import javax.inject.Inject

/**
 * Created by jyotidubey on 04/01/18.
 */
class SplashInteractor
@Inject constructor(private val mContext: Context,
                    preferenceHelper: PreferencesHelper,
                    apiHelper: ApiHelper) : BaseInteractor(preferenceHelper, apiHelper), SplashMVPInteractor {
    override fun test() {
        preferencesHelper.getUserAccessToken()

    }

}